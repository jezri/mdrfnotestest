<script> function fold(y) { var x = document.getElementById(y) ; if(x.style.display === "none"){x.style.display = "block";}else {x.style.display = "none"; } } </script><script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML' async></script><script> function fold0_0() {fold('0_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_0()"><h2>Main</h2></div>
<div class="sectionBody background-color-one" id="0_0">


This is a test document

Math

$$\, \frac{1}{2}\,$$

\begin{align*}
\alpha &= \alpha &= \alpha\\
\alpha &= \alpha &= \alpha\\
\alpha &= \alpha &= \alpha\\
\end{align*}

<script> function fold0_14_1_0() {fold('0_14_1_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_14_1_0()"><h2>Sub heading</h2></div>
<div class="sectionBody background-color-two" id="0_14_1_0">

Test


<script> function fold0_4_1_14_1_0() {fold('0_4_1_14_1_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_4_1_14_1_0()"><h2>Sub sub heading</h2></div>
<div class="sectionBody background-color-one" id="0_4_1_14_1_0">

Some test text




</div>
</div><script> function fold0_5_1_14_1_0() {fold('0_5_1_14_1_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_5_1_14_1_0()"><h2>Sub sub  sub heading</h2></div>
<div class="sectionBody background-color-one" id="0_5_1_14_1_0">

Some test text

</div>
</div>
</div>
</div>
</div>
</div><div class="section">

<div class="sectionHeading" onclick="foldAppendix()"><h2>Appendix</h2></div>
<div class="sectionBody background-color-one" id="Appendix">
</div>
</div>

</div>
