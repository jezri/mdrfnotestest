---
title : "Test"
author : "Jezri Krinsky"
---

# Imports

# Main

[printable pdf](./test.pdf)

This is a test document

Math

$$\, \frac{1}{2}\,$$

\begin{align*}
\alpha &= \alpha &= \alpha\\
\alpha &= \alpha &= \alpha\\
\alpha &= \alpha &= \alpha\\
\end{align*}

## Sub heading

Test


### Sub sub heading 

Some test text



### Sub sub  sub heading 

Some test text
