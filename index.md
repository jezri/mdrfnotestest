---
title: Markdown reference
author: Jezri Krinsky
---

# Referenced markdown

This is a webpage to demostrate the use of referenced markdown. The main conept is to make small units of reusable text anaologous to programing modules and functions which can be reused multiple times in the assembly of different documents. This is to minamize redudance and reduce updates to one section not spreading throughouot a whole document.
